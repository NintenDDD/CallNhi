﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NHI_EII_DLLLib;
using System.IO;
using System.Diagnostics;

namespace CallNhi
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            /* 影像壓縮標題 */
            Listview_7zFilelist.Columns.Add("資料夾名稱", 300, HorizontalAlignment.Center);
            Listview_7zFilelist.Columns.Add("狀態", 100, HorizontalAlignment.Center);
            Listview_7zFilelist.Columns.Add("壓縮檔名稱(含副檔名)", 300, HorizontalAlignment.Center);

            /* 檔案上傳標題 */
            Listview_Filelist.Columns.Add("檔案名稱", 400, HorizontalAlignment.Center);
            Listview_Filelist.Columns.Add("類別(共通傳輸平台)", 100, HorizontalAlignment.Center);
            Listview_Filelist.Columns.Add("狀態", 100, HorizontalAlignment.Center);
            Listview_Filelist.Columns.Add("執行編號", 100, HorizontalAlignment.Center);
            
        }

        private void add_file(string filesource){
            string filetype;
            string typecode;
            string status;

            ListViewItem newitem = new ListViewItem(filesource);

            filetype = Path.GetExtension(filesource);

            if (File.Exists(filesource))
            {
                if (filetype == ".csv")
                {
                    typecode = "90";
                    status = "收到CSV檔";
                }
                else if (filetype == ".7z" || filetype == ".7z")
                {
                    typecode = "91";
                    status = "收到壓縮檔";
                }
                else
                {
                    typecode = "00";
                    status = "檔案錯誤";
                    newitem.ForeColor = Color.Red;
                }
            }
            else {
                typecode = "00";
                status = "檔案不存在";
                newitem.ForeColor = Color.Red;
            }

            newitem.SubItems.Add(typecode);

            newitem.SubItems.Add(status);



            Listview_Filelist.Items.Add(newitem);
        }

        private void Listview_Filelist_DragDrop(object sender, DragEventArgs e)
        {
            string[] s = (string[])e.Data.GetData(DataFormats.FileDrop, false);
            int i;
            for (i = 0; i < s.Length; i++)
            {
                add_file(s[i]);
            }

        }

        private void Listview_Filelist_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
                e.Effect = DragDropEffects.All;
            else
                e.Effect = DragDropEffects.None;
        }

        private void Listview_Filelist_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                foreach (ListViewItem items in Listview_Filelist.SelectedItems)
                {
                    Listview_Filelist.Items.Remove(items);
                }
            }
        }

        private void Btn_SendNhi_Click(object sender, EventArgs e)
        {
            NHI_EIIClass nhi = new NHI_EIIClass();

            foreach (ListViewItem items in Listview_Filelist.Items)
            {
                if ((items.SubItems[1].Text == "90" || items.SubItems[1].Text == "91") && items.SubItems.Count < 4)
                {
                    items.SubItems.Add(nhi.NHI_SendA(items.SubItems[0].Text, Path.GetFileName(items.SubItems[0].Text), items.SubItems[1].Text, "", (NHI_OpMethod)337));
                    items.ForeColor = Color.Green;
                }
            }
        }

        private void Btn_CleanList_Click(object sender, EventArgs e)
        {
            Listview_Filelist.Items.Clear();
        }

        private void Btn_7zSource_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog path = new FolderBrowserDialog();
            path.ShowDialog();
            Txt_7zSource.Text = path.SelectedPath;
        }

        private void Btn_7zTarget_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog path = new FolderBrowserDialog();
            path.ShowDialog();
            Txt_7zTarget.Text = path.SelectedPath;
        }

        private void Btn_7z_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (ListViewItem items in Listview_7zFilelist.Items)
                {
                    if(items.SubItems[1].Text=="待壓縮"){
                        string[] files = Directory.GetFileSystemEntries(items.SubItems[0].Text);
                        Compress(Txt_7zTarget.Text + @"\" + Path.GetFileName(items.SubItems[0].Text) + ".7z", files);
                        items.SubItems[1].Text = "壓縮完成";
                        if (Chk_delDir.Checked == true)
                        {
                            Directory.Delete(items.SubItems[0].Text, true);
                        }
                        items.SubItems.Add(Path.GetFileName(items.SubItems[0].Text) + ".7z");
                        items.ForeColor = Color.Green;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format("Exception:{0}", ex.Message.ToString()));
            }
        }

        private void Btn_DirScan_Click(object sender, EventArgs e)
        {
            Listview_7zFilelist.Items.Clear();
            try
            {
                DirectoryInfo dir = new DirectoryInfo(Txt_7zSource.Text);

                foreach (var file in dir.GetDirectories())
                {
                    ListViewItem newitem = new ListViewItem(file.FullName);
                    if (File.Exists(file.FullName+"\\dicomdir")==true)
                    {
                        newitem.SubItems.Add("待壓縮");
                    }else{
                        newitem.SubItems.Add("找不到dicomdir");
                        newitem.ForeColor = Color.Red;
                    }
                    
                    Listview_7zFilelist.Items.Add(newitem);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format("Exception:{0}", ex.Message.ToString()));
            }
        }

        public bool Compress(string destinationName, params string[] sourceAry)
        {
            if (File.Exists(destinationName) == false)
            {
                string sourceArgs = string.Empty;
                foreach (string source in sourceAry)
                {
                    sourceArgs += "\"" + source + "\" ";
                }

                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.FileName = "7za.exe";
                startInfo.WindowStyle = ProcessWindowStyle.Hidden;

                startInfo.CreateNoWindow = true;
                startInfo.UseShellExecute = false;
                startInfo.RedirectStandardOutput = true;
                startInfo.RedirectStandardError = true;

                startInfo.Arguments = "a \"" + destinationName + "\" " + sourceArgs + "-mx=9";

                using (Process exeProcess = Process.Start(startInfo))
                {
                    string standardOutput = exeProcess.StandardOutput.ReadToEnd();
                    string errorOutput = exeProcess.StandardError.ReadToEnd();
                    exeProcess.WaitForExit();
                }

                return true;
            }
            else
            {
                return true;
            }
        }

        private void Btn_SaveSetting_Click(object sender, EventArgs e)
        {
            Properties.Settings setting = Properties.Settings.Default;
            setting.SourceDir = Txt_7zSource.Text;
            setting.TargetDir = Txt_7zTarget.Text;
            setting.DelDir = Chk_delDir.Checked;
            setting.Save();
        }

        private void Btn_CSVSource_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Title = "Select file";
            dialog.InitialDirectory = ".\\";
            dialog.Filter = "csv files (*.*)|*.csv";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                Txt_CSVSource.Text = dialog.FileName;
            }
        }

        private void Btn_InservCSV_Click(object sender, EventArgs e)
        {
            if (File.Exists(Txt_CSVSource.Text))
            {
                add_file(Txt_CSVSource.Text);
                string line;
                using (System.IO.StreamReader file = new System.IO.StreamReader(Txt_CSVSource.Text))
                {
                    while ((line = file.ReadLine()) != null)
                    {
                        string filename = "";
                        String[] substrings = line.Split(',');
                        filename = substrings[13];
                        add_file(Txt_7zTarget.Text + "\\" + filename);
                    }
                }
                tabControl1.SelectedTab = tabPage2;
            }
            else
            {
                MessageBox.Show("檔案路徑錯誤！");
            }

        }
    }
}
