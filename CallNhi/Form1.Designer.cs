﻿namespace CallNhi
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改這個方法的內容。
        ///
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.Btn_DirScan = new System.Windows.Forms.Button();
            this.Btn_7z = new System.Windows.Forms.Button();
            this.Listview_7zFilelist = new System.Windows.Forms.ListView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.Btn_CleanList = new System.Windows.Forms.Button();
            this.Btn_SendNhi = new System.Windows.Forms.Button();
            this.Listview_Filelist = new System.Windows.Forms.ListView();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.Btn_SaveSetting = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.Chk_delDir = new System.Windows.Forms.CheckBox();
            this.Btn_7zTarget = new System.Windows.Forms.Button();
            this.Btn_7zSource = new System.Windows.Forms.Button();
            this.Txt_7zTarget = new System.Windows.Forms.TextBox();
            this.Txt_7zSource = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.Btn_InservCSV = new System.Windows.Forms.Button();
            this.Btn_CSVSource = new System.Windows.Forms.Button();
            this.Txt_CSVSource = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(809, 392);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.Btn_DirScan);
            this.tabPage1.Controls.Add(this.Btn_7z);
            this.tabPage1.Controls.Add(this.Listview_7zFilelist);
            this.tabPage1.Location = new System.Drawing.Point(4, 26);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(801, 362);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "影像壓縮";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // Btn_DirScan
            // 
            this.Btn_DirScan.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_DirScan.Location = new System.Drawing.Point(226, 312);
            this.Btn_DirScan.Name = "Btn_DirScan";
            this.Btn_DirScan.Size = new System.Drawing.Size(142, 37);
            this.Btn_DirScan.TabIndex = 1;
            this.Btn_DirScan.Text = "掃描";
            this.Btn_DirScan.UseVisualStyleBackColor = true;
            this.Btn_DirScan.Click += new System.EventHandler(this.Btn_DirScan_Click);
            // 
            // Btn_7z
            // 
            this.Btn_7z.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Btn_7z.Location = new System.Drawing.Point(442, 312);
            this.Btn_7z.Name = "Btn_7z";
            this.Btn_7z.Size = new System.Drawing.Size(142, 37);
            this.Btn_7z.TabIndex = 1;
            this.Btn_7z.Text = "壓縮";
            this.Btn_7z.UseVisualStyleBackColor = true;
            this.Btn_7z.Click += new System.EventHandler(this.Btn_7z_Click);
            // 
            // Listview_7zFilelist
            // 
            this.Listview_7zFilelist.Dock = System.Windows.Forms.DockStyle.Top;
            this.Listview_7zFilelist.Font = new System.Drawing.Font("新細明體", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Listview_7zFilelist.Location = new System.Drawing.Point(3, 3);
            this.Listview_7zFilelist.Name = "Listview_7zFilelist";
            this.Listview_7zFilelist.Size = new System.Drawing.Size(795, 294);
            this.Listview_7zFilelist.TabIndex = 0;
            this.Listview_7zFilelist.UseCompatibleStateImageBehavior = false;
            this.Listview_7zFilelist.View = System.Windows.Forms.View.Details;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.Btn_CleanList);
            this.tabPage2.Controls.Add(this.Btn_SendNhi);
            this.tabPage2.Controls.Add(this.Listview_Filelist);
            this.tabPage2.Location = new System.Drawing.Point(4, 26);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(801, 362);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "檔案上傳";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // Btn_CleanList
            // 
            this.Btn_CleanList.Location = new System.Drawing.Point(442, 312);
            this.Btn_CleanList.Name = "Btn_CleanList";
            this.Btn_CleanList.Size = new System.Drawing.Size(142, 37);
            this.Btn_CleanList.TabIndex = 2;
            this.Btn_CleanList.Text = "清除";
            this.Btn_CleanList.UseVisualStyleBackColor = true;
            this.Btn_CleanList.Click += new System.EventHandler(this.Btn_CleanList_Click);
            // 
            // Btn_SendNhi
            // 
            this.Btn_SendNhi.Location = new System.Drawing.Point(226, 312);
            this.Btn_SendNhi.Name = "Btn_SendNhi";
            this.Btn_SendNhi.Size = new System.Drawing.Size(142, 37);
            this.Btn_SendNhi.TabIndex = 2;
            this.Btn_SendNhi.Text = "傳送";
            this.Btn_SendNhi.UseVisualStyleBackColor = true;
            this.Btn_SendNhi.Click += new System.EventHandler(this.Btn_SendNhi_Click);
            // 
            // Listview_Filelist
            // 
            this.Listview_Filelist.AllowDrop = true;
            this.Listview_Filelist.Dock = System.Windows.Forms.DockStyle.Top;
            this.Listview_Filelist.Font = new System.Drawing.Font("新細明體", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Listview_Filelist.Location = new System.Drawing.Point(3, 3);
            this.Listview_Filelist.Name = "Listview_Filelist";
            this.Listview_Filelist.Size = new System.Drawing.Size(795, 294);
            this.Listview_Filelist.TabIndex = 1;
            this.Listview_Filelist.UseCompatibleStateImageBehavior = false;
            this.Listview_Filelist.View = System.Windows.Forms.View.Details;
            this.Listview_Filelist.DragDrop += new System.Windows.Forms.DragEventHandler(this.Listview_Filelist_DragDrop);
            this.Listview_Filelist.DragEnter += new System.Windows.Forms.DragEventHandler(this.Listview_Filelist_DragEnter);
            this.Listview_Filelist.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Listview_Filelist_KeyDown);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.Btn_SaveSetting);
            this.tabPage3.Controls.Add(this.groupBox1);
            this.tabPage3.Location = new System.Drawing.Point(4, 26);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(801, 362);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "設定";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // Btn_SaveSetting
            // 
            this.Btn_SaveSetting.Location = new System.Drawing.Point(575, 272);
            this.Btn_SaveSetting.Name = "Btn_SaveSetting";
            this.Btn_SaveSetting.Size = new System.Drawing.Size(107, 37);
            this.Btn_SaveSetting.TabIndex = 6;
            this.Btn_SaveSetting.Text = "儲存";
            this.Btn_SaveSetting.UseVisualStyleBackColor = true;
            this.Btn_SaveSetting.Click += new System.EventHandler(this.Btn_SaveSetting_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.Chk_delDir);
            this.groupBox1.Controls.Add(this.Btn_7zTarget);
            this.groupBox1.Controls.Add(this.Btn_7zSource);
            this.groupBox1.Controls.Add(this.Txt_7zTarget);
            this.groupBox1.Controls.Add(this.Txt_7zSource);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(46, 20);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(653, 172);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "影像壓縮";
            // 
            // Chk_delDir
            // 
            this.Chk_delDir.AutoSize = true;
            this.Chk_delDir.Location = new System.Drawing.Point(231, 62);
            this.Chk_delDir.Name = "Chk_delDir";
            this.Chk_delDir.Size = new System.Drawing.Size(171, 20);
            this.Chk_delDir.TabIndex = 4;
            this.Chk_delDir.Text = "壓縮後刪除原資料夾";
            this.Chk_delDir.UseVisualStyleBackColor = true;
            // 
            // Btn_7zTarget
            // 
            this.Btn_7zTarget.Location = new System.Drawing.Point(556, 97);
            this.Btn_7zTarget.Name = "Btn_7zTarget";
            this.Btn_7zTarget.Size = new System.Drawing.Size(58, 29);
            this.Btn_7zTarget.TabIndex = 2;
            this.Btn_7zTarget.Text = "選擇";
            this.Btn_7zTarget.UseVisualStyleBackColor = true;
            this.Btn_7zTarget.Click += new System.EventHandler(this.Btn_7zTarget_Click);
            // 
            // Btn_7zSource
            // 
            this.Btn_7zSource.Location = new System.Drawing.Point(556, 27);
            this.Btn_7zSource.Name = "Btn_7zSource";
            this.Btn_7zSource.Size = new System.Drawing.Size(58, 29);
            this.Btn_7zSource.TabIndex = 2;
            this.Btn_7zSource.Text = "選擇";
            this.Btn_7zSource.UseVisualStyleBackColor = true;
            this.Btn_7zSource.Click += new System.EventHandler(this.Btn_7zSource_Click);
            // 
            // Txt_7zTarget
            // 
            this.Txt_7zTarget.Location = new System.Drawing.Point(231, 99);
            this.Txt_7zTarget.Name = "Txt_7zTarget";
            this.Txt_7zTarget.Size = new System.Drawing.Size(314, 27);
            this.Txt_7zTarget.TabIndex = 1;
            this.Txt_7zTarget.Text = "C:\\Target";
            // 
            // Txt_7zSource
            // 
            this.Txt_7zSource.Location = new System.Drawing.Point(231, 29);
            this.Txt_7zSource.Name = "Txt_7zSource";
            this.Txt_7zSource.Size = new System.Drawing.Size(314, 27);
            this.Txt_7zSource.TabIndex = 1;
            this.Txt_7zSource.Text = "C:\\Source";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label2.Location = new System.Drawing.Point(89, 105);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(136, 16);
            this.label2.TabIndex = 0;
            this.label2.Text = "壓縮目的資料夾：";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label1.Location = new System.Drawing.Point(89, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(136, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "壓縮來源資料夾：";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.Btn_CSVSource);
            this.tabPage4.Controls.Add(this.Txt_CSVSource);
            this.tabPage4.Controls.Add(this.label4);
            this.tabPage4.Controls.Add(this.label3);
            this.tabPage4.Controls.Add(this.Btn_InservCSV);
            this.tabPage4.Location = new System.Drawing.Point(4, 26);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(801, 362);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "CSV>>檔案上傳";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // Btn_InservCSV
            // 
            this.Btn_InservCSV.Location = new System.Drawing.Point(312, 120);
            this.Btn_InservCSV.Name = "Btn_InservCSV";
            this.Btn_InservCSV.Size = new System.Drawing.Size(158, 38);
            this.Btn_InservCSV.TabIndex = 3;
            this.Btn_InservCSV.Text = "匯入CSV&檔案";
            this.Btn_InservCSV.UseVisualStyleBackColor = true;
            this.Btn_InservCSV.Click += new System.EventHandler(this.Btn_InservCSV_Click);
            // 
            // Btn_CSVSource
            // 
            this.Btn_CSVSource.Location = new System.Drawing.Point(590, 46);
            this.Btn_CSVSource.Name = "Btn_CSVSource";
            this.Btn_CSVSource.Size = new System.Drawing.Size(58, 29);
            this.Btn_CSVSource.TabIndex = 6;
            this.Btn_CSVSource.Text = "選擇";
            this.Btn_CSVSource.UseVisualStyleBackColor = true;
            this.Btn_CSVSource.Click += new System.EventHandler(this.Btn_CSVSource_Click);
            // 
            // Txt_CSVSource
            // 
            this.Txt_CSVSource.Location = new System.Drawing.Point(265, 48);
            this.Txt_CSVSource.Name = "Txt_CSVSource";
            this.Txt_CSVSource.Size = new System.Drawing.Size(314, 27);
            this.Txt_CSVSource.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label3.Location = new System.Drawing.Point(123, 54);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(117, 16);
            this.label3.TabIndex = 4;
            this.label3.Text = "CSV檔案路徑：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label4.Location = new System.Drawing.Point(123, 207);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(325, 16);
            this.label4.TabIndex = 4;
            this.label4.Text = "＊從壓縮目的資料夾讀取CSV內的壓縮檔檔名";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(809, 392);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.Text = "健保局API整理程式(影像上傳)";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.ListView Listview_Filelist;
        private System.Windows.Forms.Button Btn_SendNhi;
        private System.Windows.Forms.Button Btn_CleanList;
        private System.Windows.Forms.ListView Listview_7zFilelist;
        private System.Windows.Forms.Button Btn_7z;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox Txt_7zTarget;
        private System.Windows.Forms.TextBox Txt_7zSource;
        private System.Windows.Forms.Button Btn_7zTarget;
        private System.Windows.Forms.Button Btn_7zSource;
        private System.Windows.Forms.Button Btn_DirScan;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox Chk_delDir;
        private System.Windows.Forms.Button Btn_SaveSetting;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Button Btn_CSVSource;
        private System.Windows.Forms.TextBox Txt_CSVSource;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button Btn_InservCSV;
        private System.Windows.Forms.Label label4;
    }
}

